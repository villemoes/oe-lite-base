#!/bin/sh -e

manifest_dir="$(dirname $0)/manifest"
project_dir="${CI_PROJECT_DIR:-$(pwd)}"

mkdir -p "$manifest_dir"
cd "$manifest_dir"
mkdir -p conf meta

git clone https://gitlab.com/oe-lite/core.git meta/core
echo "OESTACK = \"meta/core\"" > conf/bakery.conf

ln -s "$project_dir" meta/base
echo "OESTACK += \"meta/base\"" >> conf/bakery.conf

for layer in "$@" ; do
    git clone https://gitlab.com/oe-lite/$layer.git meta/$layer
    echo "OESTACK += \"meta/$layer\"" >> conf/bakery.conf
done

echo "Manifest signature:"
for d in meta/* ; do
    echo -n "$d "
    (cd "$d" && git rev-parse --verify HEAD)
done

cat > conf/auto.conf <<EOF
PARALLEL_MAKE = "-j $(nproc)"
RMWORK = "1"
EOF
