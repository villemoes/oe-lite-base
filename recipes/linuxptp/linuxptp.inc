inherit c make

SRC_URI = "git://git.code.sf.net/p/linuxptp/code;protocol=https;${SRC_REV}"
S = "${SRCDIR}/code"

COMPATIBLE_HOST_ARCHS = ".*linux"

EXTRA_OEMAKE += "CROSS_COMPILE=${TARGET_PREFIX} prefix=${prefix} mandir=${mandir}"

DEPENDS = "librt libm"

inherit auto-package-utils
AUTO_PACKAGE_UTILS = "phc_ctl pmc hwstamp_ctl timemaster ptp4l phc2sys nsm"
AUTO_PACKAGE_UTILS_DEPENDS = "librt libm"
AUTO_PACKAGE_UTILS_RDEPENDS = "librt libm"

RDEPENDS_${PN} = "${AUTO_PACKAGE_UTILS_PROVIDES}"
