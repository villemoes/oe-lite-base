DESCRIPTION = "libsoup is an HTTP client/server library for GNOME."
HOMEPAGE = "LGPL-2.0-or-later"

RECIPE_TYPES = "machine"
COMPATIBLE_HOST_ARCHS = ".*linux"

require conf/fetch/gnome.conf
SRC_URI_SUBDIR = "${@'.'.join(d.getVar('PV', True).split('.')[:2])}"
SRC_URI = "${GNOME_MIRROR}/${PN}/${SRC_URI_SUBDIR}/${PN}-${PV}.tar.xz"

inherit autotools library pkgconfig

DEPENDS = "libglib-2.0 libgobject-2.0 native:glib-utils"
DEPENDS += "libxml2 libsqlite3 libpsl libgio"

DEPENDS_${PN} += "libxml2 libsqlite3 libpsl libgio"
RDEPENDS_${PN} += "libxml2 libsqlite3 libpsl libgio"

EXTRA_OECONF += "--without-gnome"

FILES_${PN}-doc += "${datadir}/gtk-doc"
